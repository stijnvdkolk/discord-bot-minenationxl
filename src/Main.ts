import { DiscordBot } from "./DiscordBot";

export class Main {

    private readonly token: string;

    constructor(token: string) {

        this.token = token;

    }

    public async boot() {

        // Initialize Discord bot
        let discord = new DiscordBot(this.token);
        await discord.start();

    }

}