import { DiscordBot } from '../DiscordBot';
import AbstractCommand from '../commands/AbstractCommand';
import { Message } from 'discord.js';
import chalk from '../../node_modules/chalk';

export default async function onMessage(msg: Message) {

    // Check command
    if (!msg.content.startsWith("!")) {
        return;
    }

    // Check for valid command
    const command: string = msg.content.split(" ")[0].substring(1);
    const args: string[] = msg.content.split(" ").slice(1);

    if (!DiscordBot.commands.has(command)) {
        return;
    }

    const finalCommand: AbstractCommand = DiscordBot.commands.get(command).setMessage(msg).setArgs(args);

    console.info(`${chalk.green("[INFO]")} Executing '!${command}' for user ${msg.author.id}`);

    // Execute the command
    finalCommand.execute();

}