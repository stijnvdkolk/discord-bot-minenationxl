import AbstractCommand from "./AbstractCommand";

export default class PingCommand extends AbstractCommand {

    name = "ping";

    public execute() {

        this.message.channel.send(":coffee: **Pong**, uw koffie is klaar!");

    }

}