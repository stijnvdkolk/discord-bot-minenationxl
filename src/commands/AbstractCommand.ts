import { Message, User, Channel } from 'discord.js';

export default abstract class AbstractCommand {

    public name: String;
    public message: Message;
    public args: String[];

    public setMessage(message: Message): this {
        
        this.message = message;
        return this;

    }

    public setArgs(args: String[]): this {

        this.args = args;
        return this;

    }

    public execute() { }

}